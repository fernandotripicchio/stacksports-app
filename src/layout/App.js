import React from 'react';
import Nav from './components/Nav';
import AppContent from './AppContent';
import './app.css';

class App extends React.Component {
  render() {
    return(
      <div className="container">
        <Nav />
        <div className="content-wrapper">
            <AppContent { ...this.props } />
        </div>        
      </div>
    );
  }
}

export default App;
