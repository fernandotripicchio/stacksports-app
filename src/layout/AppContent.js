import React from "react";
import { Switch, Route, Redirect } from "react-router-dom";
import LeaguesIndex from '../pages/leagues/LeagueIndex/LeagueConnector';
import LeaguesNewForm from '../pages/leagues/LeagueCreate/LeagueConnector';
import TeamsIndex from '../pages/teams/TeamIndex/TeamConnector';
import TeamsNewForm from '../pages/teams/TeamCreate/TeamConnector';
import PlayersIndex from '../pages/players/PlayerIndex/PlayerConnector';
import PlayersNewForm from '../pages/players/PlayerCreate/PlayerConnector';

class AppContent extends React.Component {
    render() {
      return (
        <Switch>
          <Route
            path="/leagues/new"
            component={LeaguesNewForm}
          />               
          <Route
            path="/leagues/teams/new/:league_id"
            component={TeamsNewForm}
          />               
          <Route
            path="/leagues/teams/:league_id"
            component={TeamsIndex}
          />
          <Route
            path="/players/:league_id/:team_id"
            component={PlayersIndex}
          />          
          <Route
            path="/new_player/:league_id/:team_id"
            component={PlayersNewForm}
          />          

          <Route
            path="/leagues"
            component={LeaguesIndex}
          />     
          <Redirect to="/leagues" />
        </Switch>
      );
    }
  }
  
  export default AppContent;
  