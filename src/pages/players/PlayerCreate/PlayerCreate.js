import React from 'react';
import { 
  Container, 
  Row 
} from 'reactstrap';

class PlayerForm extends React.Component {
  constructor () {
  super()
    this.state = {
      first_name: '',
      last_name: ''
    }
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }    

  handleSubmit() {
    console.log("HandleSubmit");
    let leagueId = this.props.match.params.league_id;
    let teamId = this.props.match.params.team_id;
    const data = {
      league_id: leagueId,
      team_id: teamId,
      player: {
        first_name: this.state.first_name,
        last_name: this.state.last_name,
        speed: 10,
        agility: 10,
        strength: 10
      }
    };
    this.props.addPlayer(data).then(
      response => {
        this.props.player.players.push(data);
      }  
    );
    this.props.history.push("/players/"+ leagueId+"/"+teamId);
  }

  handleChange(e) {
    let change = {}
    console.log(e.target);
    change[e.target.name] = e.target.value
    this.setState(change)
  }

  render() {
    return(
      <Container>
        <h3 className="pageHeaderLabel">New Player</h3>
        <hr />
        <Row>
          <form onSubmit={this.handleSubmit}>
            <Container>
              <Row>
                <input required name="first_name" onChange={this.handleChange.bind(this)} placeholder="first name" /><br /><br />
              </Row>  
              <Row>
                <input required name="last_name" onChange={this.handleChange.bind(this)} placeholder="last name" /><br /><br />
              </Row>  
              <Row>
                <button>Create</button>
              </Row>
            </Container>
          </form>
        </Row>
      </Container>        
    );
  }
}
  
export default PlayerForm;