import { connect } from 'react-redux';
import PlayerCreatePage from './PlayerCreate';
import * as playerActions from '../../../reducers/player/playerActions';

export default connect(
  (state) => {return state},playerActions)(PlayerCreatePage)