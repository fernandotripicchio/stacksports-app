import React from 'react';
import { Link } from 'react-router-dom'
import { 
  Container, 
  Row, 
  Col,
  CardBody,
  Alert,
  Card  
} from 'reactstrap';
import Player from './Player';

class PlayerIndex extends React.Component {  

  constructor () {
    super()
    this.state = {
      players: [],
      working: true
    };  
  }  

  componentWillMount(){
    let data = {
      league_id: this.props.match.params.league_id,
      team_id: this.props.match.params.team_id
    }
    this.props.actions.fetchPlayers(data).then(() => {
      this.setState({ players: this.props.players, working: false });
    });
  }  

  render() {
    let {players} = this.state;
    return(
      <Container>
        <h3 className="pageHeaderLabel">Player</h3>
        <br />
        <Link className="nav-link"  to={`/new_player/${this.props.match.params.league_id}/${this.props.match.params.team_id}`}>New Player</Link>
        <hr />
        { 
          players.length === 0 ? (
            <Card className="mb-4">
              <CardBody>
                <Alert color="secondary">
                  No Players to show...
                </Alert>
              </CardBody>
            </Card>              
          ) : (  
            <div>
              <Row>  
                <Col xs="3">Id</Col>
                <Col xs="3">First Name</Col>
                <Col xs="3">Last Name</Col>
                <Col></Col>
                <Col></Col>
              </Row>
              {players.map((player, index) => (
                <Player key={index} player={player} />
              ))}                
            </div>
          )
        }          
      </Container>        
    );
  }
}
  
export default PlayerIndex;
