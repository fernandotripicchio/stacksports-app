import { connect } from 'react-redux';
import { bindActionCreators } from "redux";
import PlayerIndexPage from './PlayerIndex';
import * as teamActions from '../../../reducers/player/playerActions';

function mapStateToProps(state) {
  console.log("State players ", state);
  return {
    errorMessage: state.team.errorMessage,
    working: state.player.working,
    players: state.player.players
  };
}
      
function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(
      Object.assign({}, teamActions),
      dispatch
    )
  };
}
     
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PlayerIndexPage);    