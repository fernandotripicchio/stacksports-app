import React, { Component } from 'react';
import { Link } from 'react-router-dom'

import { 
    Row, 
    Col 
} from 'reactstrap';

class Player extends Component {
  render() {
    const {player} = this.props;  
    return (
      <Row>
        <Col xs="3">{player.id}</Col>
        <Col xs="3">{player.last_name}</Col>
        <Col xs="3">{player.first_name}</Col>
        <Col><button>Edit</button></Col>
        <Col><button>Delete</button></Col>        
      </Row>
    )
  }
}

export default Player;