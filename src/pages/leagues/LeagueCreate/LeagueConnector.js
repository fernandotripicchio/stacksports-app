import { connect } from 'react-redux';
import LeagueCreatePage from './LeagueCreate';
import * as leagueActions from '../../../reducers/league/leagueActions';

export default connect(
  (state) => {return state},leagueActions)(LeagueCreatePage)