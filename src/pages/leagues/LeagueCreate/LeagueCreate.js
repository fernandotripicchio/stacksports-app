import React from 'react';
import { 
  Container, 
  Row 
} from 'reactstrap';

class LeagueForm extends React.Component {
  constructor () {
  super()
    this.state = {
      name: ''
    }
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);      
  }    

  handleSubmit() {
    console.log("Submitting form");  
    const data = {
      league:{
        name: this.state.name 
      }
    };
    this.props.addLeague(data).then(
      response => {
        this.props.league.leagues.push(data);
      }  
    );
    this.props.history.push('/leagues');
  }

  handleChange(input) {
    this.setState({ name: input.target.value });
  }

  render() {
    return(
      <Container>
        <h3 className="pageHeaderLabel">New League</h3>
        <hr />
        <Row>
          <form onSubmit={this.handleSubmit}>
            <Container>
              <Row>
                <input required type="text" onChange={ this.handleChange } placeholder="league name" /><br /><br />
              </Row>  
              <Row>
                <button>Create</button>
              </Row>
            </Container>
          </form>
        </Row>
      </Container>        
    );
  }
}
  
export default LeagueForm;