import React from 'react';
import { 
  Container, 
  Row, 
  Col,
  CardBody,
  Alert,
  Card  
} from 'reactstrap';
import League from './League';

class LeagueIndex extends React.Component {

  constructor () {
    super()
    this.state = {
      leagues: [],
      working: true
    };  
  }   

  componentWillMount() {
    this.props.actions.fetchLeagues().then(() => {
      this.setState({ leagues: this.props.leagues, working: false });
    });
  }

  render() {
    let {leagues, working} = this.state;

    return(
      <Container>
        <h3 className="pageHeaderLabel">Leagues</h3>
        <hr />
        {
          working ? ( " Loading ") : (
          leagues.length === 0 ? (
            <Card className="mb-4">
              <CardBody>
                <Alert color="secondary">
                  No Legues to show...
                </Alert>
              </CardBody>
            </Card>              
          ) : (  
            <div>
              <Row>  
                <Col xs="3">Id</Col>
                <Col xs="3">Name</Col>
                <Col></Col>
                <Col></Col>
              </Row>
              {leagues.map((league, index) => (
                <League key={index} league={league} />
              ))}                
            </div>
          )
      )}  
    </Container>        
   )};
  }
  
  export default LeagueIndex;
