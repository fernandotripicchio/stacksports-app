import { connect } from 'react-redux';
import { bindActionCreators } from "redux";
import LeagueIndexPage from './LeagueIndex';
import * as leagueActions from '../../../reducers/league/leagueActions';

function mapStateToProps(state) {
  return {
    errorMessage: state.league.errorMessage,
    working: state.league.working,
    leagues: state.league.leagues
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(
      Object.assign({}, leagueActions),
      dispatch
    )
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LeagueIndexPage);