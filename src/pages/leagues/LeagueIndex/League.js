import React, { Component } from 'react';
import { Link } from 'react-router-dom'

import { 
    Row, 
    Col 
} from 'reactstrap';

class League extends Component {
  render() {
    const {league} = this.props;  
    return (
      <Row>
        <Col xs="3">{league.id}</Col>
        <Col xs="3">{league.name}</Col>
        <Col><Link to={`/leagues/teams/${league.id}`}>Teams</Link></Col>
        <Col><button>Edit</button></Col>
        <Col><button>Delete</button></Col>        
      </Row>
    )
  }
}

export default League;