import { connect } from 'react-redux';
import { bindActionCreators } from "redux";
import TeamIndexPage from './TeamIndex';
import * as teamActions from '../../../reducers/team/teamActions';

function mapStateToProps(state) {
  console.log("State teams ", state);
  return {
    errorMessage: state.team.errorMessage,
    working: state.team.working,
    teams: state.team.teams
  };
}
      
function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(
      Object.assign({}, teamActions),
      dispatch
    )
  };
}
     
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TeamIndexPage);    