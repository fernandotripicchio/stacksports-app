import React from 'react';
import { Link } from 'react-router-dom'
import { 
  Container, 
  Row, 
  Col,
  CardBody,
  Alert,
  Card  
} from 'reactstrap';
import Team from './Team';

class TeamIndex extends React.Component {  

  constructor () {
    super()
    this.state = {
      teams: [],
      working: true
    };  
  }  

  componentWillMount(){
    this.props.actions.fetchTeams(this.props.match.params.league_id).then(() => {
      this.setState({ teams: this.props.teams, working: false });
    });
  }  

  render() {
    let {teams} = this.state;
    return(
      <Container>
        <h3 className="pageHeaderLabel">Team</h3>
        <br />
        <Link className="nav-link"  to={`/leagues/teams/new/${this.props.match.params.league_id}`}>New Team</Link>
        <hr />
        { 
          teams.length === 0 ? (
            <Card className="mb-4">
              <CardBody>
                <Alert color="secondary">
                  No Teams to show...
                </Alert>
              </CardBody>
            </Card>              
          ) : (  
            <div>
              <Row>  
                <Col xs="3">Id</Col>
                <Col xs="3">Name</Col>
                <Col></Col>
                <Col></Col>
              </Row>
              {teams.map((team, index) => (
                <Team key={index} team={team} />
              ))}                
            </div>
          )
        }          
      </Container>        
    );
  }
}
  
export default TeamIndex;
