import React, { Component } from 'react';
import { Link } from 'react-router-dom'

import { 
    Row, 
    Col 
} from 'reactstrap';

class Team extends Component {
  render() {
    const {team} = this.props;  
    console.log("Team ", team);
    return (
      <Row>
        <Col xs="3">{team.id}</Col>
        <Col xs="3">{team.name}</Col>
        <Col><Link to={`/players/${team.league_id}/${team.id}`}>Players</Link></Col>
        <Col><button>Edit</button></Col>
        <Col><button>Delete</button></Col>        
      </Row>
    )
  }
}

export default Team;