import React from 'react';
import { 
  Container, 
  Row 
} from 'reactstrap';

class TeamForm extends React.Component {
  constructor () {
  super()
    this.state = {
      name: ''
    }
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);      
  }    

  handleSubmit() {
    let leagueId = this.props.match.params.league_id;
    const data = {
      league_id: leagueId,
      team: {
        name: this.state.name       
      }
    };
    this.props.addTeam(data).then(
      response => {
        this.props.team.teams.push(data);
      }  
    );
    this.props.history.push("/leagues/teams/"+ leagueId);
  }

  handleChange(input) {
    this.setState({ name: input.target.value });
  }

  render() {
    return(
      <Container>
        <h3 className="pageHeaderLabel">New Team</h3>
        <hr />
        <Row>
          <form onSubmit={this.handleSubmit}>
            <Container>
              <Row>
                <input required type="text" onChange={ this.handleChange } placeholder="team name" /><br /><br />
              </Row>  
              <Row>
                <button>Create</button>
              </Row>
            </Container>
          </form>
        </Row>
      </Container>        
    );
  }
}
  
export default TeamForm;