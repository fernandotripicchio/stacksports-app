import { connect } from 'react-redux';
import TeamCreatePage from './TeamCreate';
import * as teamActions from '../../../reducers/team/teamActions';

export default connect(
  (state) => {return state},teamActions)(TeamCreatePage)