import request from '../../shared/helpers/request';
import * as actionsTypes from "./leagueConstant";

export function fetchLeaguesSuccess(data) {
    return { type: actionsTypes.FETCH_LEAGUES_SUCCESS, data };
}
  
export function fetchLeaguesFailure(errorMessage) {
    return { type: actionsTypes.FETCH_LEAGUE_FAILURE, errorMessage };
}

export function fetchLeagues(opts) {
  return function(dispatch) {
    return request.get("/leagues.json", opts, dispatch).then(
      response => {
        dispatch(fetchLeaguesSuccess(response.data));
      },
      error => {
        dispatch(fetchLeaguesFailure(error));
      }
    );
  };
}

export function addLeagueSuccess(data) {
  return { type: actionsTypes.ADD_LEAGUE_SUCCESS, data };
}

export function addLeagueFailure(errorMessage) {
  return { type: actionsTypes.ADD_LEAGUE_FAILURE, errorMessage };
}

export function addLeague(league) {
  return function (dispatch) {
    return request.post('/leagues.json', league, dispatch).then(
      response => {
        dispatch(addLeagueSuccess(response.data));
      },
      error => {
        dispatch(addLeagueFailure(error));
      }
    );
  };
}