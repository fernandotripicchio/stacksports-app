import * as actionsTypes from './leagueConstant';

const initialState = {
    leagues: [],
    fetchingLeague: false,
    errorMessage: '',
    working: true
  };

const usableState = Object.assign({}, initialState);

export let leagueReducer = (state = usableState,action) => {
    let editedState;
    switch (action.type) {
      case actionsTypes.FETCHING_LEAGUES:
        editedState = Object.assign({}, state);
        editedState.fetchingLeague = true;
        editedState.working = false;
        break;
  
      case actionsTypes.FETCH_LEAGUES_SUCCESS:
        editedState = Object.assign({}, state);
        editedState.leagues = action.data;
        editedState.fetchingLeague = false;
        break;

      case actionsTypes.FETCH_LEAGUE_FAILURE:
        editedState = Object.assign({}, state);
        editedState.fetchingLeague = false;
        editedState.fetchingErrorMessage = action.errorMessage;
        break;

      case actionsTypes.ADD_LEAGUE:
        editedState = Object.assign({}, state);
        editedState.addingLeague = true;
        break;
  
      case actionsTypes.ADD_LEAGUE_SUCCESS:
        editedState = Object.assign({}, state);
        editedState.addingLeague = false;
        break;
  
      case actionsTypes.ADD_LEAGUE_FAILURE:
        editedState = Object.assign({}, state);
        editedState.addingLeague = false;
        editedState.addingErrorMessage = action.addingErrorMessage;
  
        break;
      default:
        return state;
    }
    return editedState;    
};