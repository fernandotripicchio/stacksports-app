import { combineReducers } from 'redux';
import {leagueReducer} from './league/leagueReducer';
import {teamReducer} from './team/teamReducer';
import {playerReducer} from './player/playerReducer';

const rootReducer = combineReducers({
  league: leagueReducer,
  team: teamReducer,
  player: playerReducer
});

export default rootReducer;