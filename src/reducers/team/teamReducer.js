import * as actionsTypes from './teamConstant';

const initialState = {
    teams: [],
    fetchingTeam: false,
    errorMessage: ''
  };

const usableState = Object.assign({}, initialState);

export let teamReducer = (state = usableState,action) => {
    let editedState;
    switch (action.type) {
      case actionsTypes.FETCHING_TEAMS:
        editedState = Object.assign({}, state);
        editedState.fetchingTeam = true;
        break;
  
      case actionsTypes.FETCH_TEAMS_SUCCESS:
        editedState = Object.assign({}, state);
        editedState.teams = action.data;
        editedState.fetchingTeam = false;
        break;

      case actionsTypes.FETCH_TEAM_FAILURE:
        editedState = Object.assign({}, state);
        editedState.fetchingTeam = false;
        editedState.fetchingErrorMessage = action.errorMessage;
        break;

      case actionsTypes.ADD_TEAM:
        editedState = Object.assign({}, state);
        editedState.addingTeam = true;
        break;
  
      case actionsTypes.ADD_TEAM_SUCCESS:
        editedState = Object.assign({}, state);
        editedState.addingTeam = false;
        break;
  
      case actionsTypes.ADD_TEAM_FAILURE:
        editedState = Object.assign({}, state);
        editedState.addingTeam = false;
        editedState.addingErrorMessage = action.addingErrorMessage;
  
        break;
      default:
        return state;
    }
    return editedState;    
};