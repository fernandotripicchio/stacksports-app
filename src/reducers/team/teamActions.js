import request from '../../shared/helpers/request';
import * as actionsTypes from "./teamConstant";

export function fetchTeamsSuccess(data) {
    return { type: actionsTypes.FETCH_TEAMS_SUCCESS, data };
}
  
export function fetchTeamsFailure(errorMessage) {
    return { type: actionsTypes.FETCH_TEAM_FAILURE, errorMessage };
}

export function fetchTeams(opts) {
  return function(dispatch) {
    return request.get("/leagues/"+opts+"/teams.json", opts, dispatch).then(
      response => {
        dispatch(fetchTeamsSuccess(response.data));
      },
      error => {
        dispatch(fetchTeamsFailure(error));
      }
    );
  };
}

export function addTeamsuccess(data) {
  return { type: actionsTypes.ADD_TEAM_SUCCESS, data };
}

export function addTeamFailure(errorMessage) {
  return { type: actionsTypes.ADD_TEAM_FAILURE, errorMessage };
}

export function addTeam(data) {
  return function (dispatch) {
    return request.post("/leagues/"+data["league_id"]+"/teams.json", data, dispatch).then(
      response => {
        dispatch(addTeamsuccess(response.data));
      },
      error => {
        dispatch(addTeamFailure(error));
      }
    );
  };
}