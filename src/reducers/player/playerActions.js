import request from '../../shared/helpers/request';
import * as actionsTypes from "./playerConstant";

export function fetchPlayersSuccess(data) {
    return { type: actionsTypes.FETCH_PLAYERS_SUCCESS, data };
}
  
export function fetchPlayersFailure(errorMessage) {
    return { type: actionsTypes.FETCH_PLAYER_FAILURE, errorMessage };
}

export function fetchPlayers(opts) {
  return function(dispatch) {
    return request.get("/leagues/"+opts["league_id"]+"/teams/"+opts["team_id"]+"/players.json", opts, dispatch).then(
      response => {
        dispatch(fetchPlayersSuccess(response.data));
      },
      error => {
        dispatch(fetchPlayersFailure(error));
      }
    );
  };
}

export function addPlayersuccess(data) {
  return { type: actionsTypes.ADD_PLAYER_SUCCESS, data };
}

export function addPlayerFailure(errorMessage) {
  return { type: actionsTypes.ADD_PLAYER_FAILURE, errorMessage };
}

export function addPlayer(data) {
  return function (dispatch) {
    return request.post("/leagues/"+data["league_id"]+"/teams/"+data["team_id"]+"/players.json", data, dispatch).then(
      response => {
        dispatch(addPlayersuccess(response.data));
      },
      error => {
        dispatch(addPlayerFailure(error));
      }
    );
  };
}