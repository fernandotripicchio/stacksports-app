import * as actionsTypes from './playerConstant';

const initialState = {
    players: [],
    fetchingPlayer: false,
    errorMessage: ''
  };

const usableState = Object.assign({}, initialState);

export let playerReducer = (state = usableState,action) => {
    let editedState;
    switch (action.type) {
      case actionsTypes.FETCHING_PLAYERS:
        editedState = Object.assign({}, state);
        editedState.fetchingPlayer = true;
        break;
  
      case actionsTypes.FETCH_PLAYERS_SUCCESS:
        editedState = Object.assign({}, state);
        editedState.players = action.data;
        editedState.fetchingPlayer = false;
        break;

      case actionsTypes.FETCH_PLAYER_FAILURE:
        editedState = Object.assign({}, state);
        editedState.fetchingPlayer = false;
        editedState.fetchingErrorMessage = action.errorMessage;
        break;

      case actionsTypes.ADD_PLAYER:
        editedState = Object.assign({}, state);
        editedState.addingPlayer = true;
        break;
  
      case actionsTypes.ADD_PLAYER_SUCCESS:
        editedState = Object.assign({}, state);
        editedState.addingPlayer = false;
        break;
  
      case actionsTypes.ADD_PLAYER_FAILURE:
        editedState = Object.assign({}, state);
        editedState.addingPlayer = false;
        editedState.addingErrorMessage = action.addingErrorMessage;
  
        break;
      default:
        return state;
    }
    return editedState;    
};