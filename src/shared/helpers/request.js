// Packages
import axios from "axios";

const axiosInstance = axios.create();

// LocalHost configs
const localApiUrl = "http://localhost:3000";
const baseUrl = `${localApiUrl}/api/v1/`;

// Config Axios
axiosInstance.defaults.baseURL = baseUrl;

export default class Request {
  static get(path, params, dispatch) {
    if (dispatch) {
      return axiosInstance.get(path, { params }, { dispatch });
    } else {
      return axiosInstance.get(path, { dispatch });
    }
  }

  static post(path, data = {}, dispatch) {
    return axiosInstance.post(path, data, { dispatch });
  }

  static put(path, data = {}, dispatch) {
    return axiosInstance.put(path, data, { dispatch });
  }

  static delete(path, data = {}, dispatch) {
    return axiosInstance.delete(path, data, { dispatch });
  }

  static patch(path, data = {}, dispatch) {
    return axiosInstance.patch(path, data, { dispatch });
  }
}
