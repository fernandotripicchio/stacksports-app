import React from 'react';
import { render } from 'react-dom';
import { AppContainer } from 'react-hot-loader';
import { BrowserRouter as Router } from 'react-router-dom';
import App from './layout/App';
import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import thunk from 'redux-thunk';
import reducers from './reducers';

const createStoreWithMiddleware = applyMiddleware(thunk)(createStore);

render((
    <AppContainer>
       <Provider store={createStoreWithMiddleware(reducers)}>
        <Router>
            <App />
        </Router>
       </Provider>
    </AppContainer>
  ), document.getElementById('root'))